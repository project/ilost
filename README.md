# README #

This README describes the iLost module.

### What is this repository for? ###

* Create API connection to iLost service.
* Version 7.x

### How do I get set up? ###

* Create account at iLost.
* Enter your iLost details on the settings page.

### How to get your api credentials.
Login at https://demo-match.ilost.co and navigate
to https://demo-match.ilost.co/api/session

### Who do I talk to? ###

* https://www.drupal.org/u/edwin-knol
