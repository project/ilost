<?php

/**
 * @file
 * Default theme implementation to display a Ilist block.
 *
 * Available variables:
 * - $variables['ilost']
 * - $variables['ilost-list']
 */
?>

<?php foreach ($variables['ilost']['items'] as $item): ?>
  <div class="ilost-items">
    <div class="ilost-item">
      <div class='ilost-title'><a href ='<?php print $item->webUrl ?>'><?php print check_plain($item->title); ?></a></div>
      <div class="ilost-organization"><?php print $item->organization ?>, <span class="ilost-location"><?php print $item->location->description?></span></div>
      <div class="ilost-date"><?php print $item->reportedDate ?></div>
    </div>
  </div>
<?php endforeach; ?>
