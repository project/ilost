<?php

/**
 * @file
 * Template file.
 */
?>


    <?php if (isset($note)): ?>
        <div><p class="note"><?php print $note; ?></p></div>
    <?php endif; ?>

<div>
    <?php if (isset($search_text)): ?>
        <div><?php print $search_text; ?></div>
    <?php endif; ?>
    <?php if (isset($search_results)): ?>
        <div><?php print $search_results; ?></div>
    <?php endif; ?>
</div>

    <?php if (isset($children)): ?>
        <div><?php print $children; ?></div>
    <?php endif; ?>
