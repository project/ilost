<?php

/**
 * @file
 * Search form.
 */

/**
 * Search form.
 */
function ilost_search_form($form, &$form_state) {

  $form['search-text'] = array(
    '#type' => 'textfield',
    '#title' => t('Search'),
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => 'ilost_ajax_submit_callback',
      'wrapper' => 'box',
      'keypress' => TRUE,
    ),
    '#attributes' => array('class' => array('use-ajax-submit')),
  );

  $form['box'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="box">',
    '#suffix' => '</div>',
    // '#markup' => ilost_widget_item_list(),
    '#markup' => theme('ilost_list_items', ilost_widget_render()),
  );

  return $form;
}

/**
 * Callback for our form.
 *
 * @return array
 *   Render array (the box element)
 */
function ilost_ajax_submit_callback($form, $form_state) {
  $element = $form['box'];
  if (!empty($form_state['values']['search-text'])) {
    $result = ilost_search_result($form_state['values']['search-text']);
    if ($result) {
      $variables = $result;
      $element['#markup'] = theme('ilost_list_items', $variables);
    }
    else {
      $element['#markup'] = 'No item found.';
    }
  }
  return $element;
}

/**
 * Return search result.
 */
function ilost_search_result($value) {
  $list = ilost_widget_render();
  $items = array();
  foreach ($list['ilost']['items'] as $item) {
    if (stripos($item->title, $value) !== FALSE) {
      $items[] = $item;
    }
  }

  if ($items) {
    $records = array(
      'ilost' => array(
        'items' => $items,
      ),
    );
  }
  else {
    $records = NULL;
  }

  return $records;

}

/**
 * Implements hook_preprocess_form_id().
 */
function ilost_preprocess_ilost_search_form(&$variables) {
  // Shorten form variable.
  $form = $variables['form'];

  // Change labels for the search-text.
  $form['search-text']['#title'] = t('Search for lost items.');

  // Create a new variable for a custom note.
  $variables['note'] = variable_get('ilost_note');

  // Create variables for individual elements.
  $variables['search_text'] = render($form['search-text']);
  $variables['search_results'] = render($form['box']);

  // Print the remaining rendered form items.
  $variables['children'] = drupal_render_children($form);
}
