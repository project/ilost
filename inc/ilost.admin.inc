<?php

/**
 * @file
 * Administration form for ILost module.
 */

/**
 * ILost setting form.
 */
function ilost_admin_settings($form) {

  $form['info']['title'] = array(
    '#markup' => t('<p><em>ILost admin settings.</em></p>'),
  );

  $form['ilost_organization'] = array(
    '#type' => 'fieldset',
    '#title' => t('Organization'),
  );

  $form['ilost_organization']['id'] = array(
    '#type' => 'textfield',
    '#title' => t('Organization Id'),
    '#default_value' => variable_get('ilost_organization_id'),
    '#size' => 5,
    '#maxlength' => 5,
    '#description' => t("Providing the organization's numeric ID"),
  );

  $form['ilost_organization']['slug'] = array(
    '#type' => 'textfield',
    '#title' => t('Organization Slug'),
    '#default_value' => variable_get('ilost_organization_slug'),
    '#size' => 80,
    '#maxlength' => 60,
    '#description' => t("Providing the organization's slug"),
  );

  $form['ilost_api'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Settings'),
  );

  $form['ilost_api']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('Url'),
    '#default_value' => variable_get('ilost_api_url'),
    '#size' => 120,
    '#maxlength' => 60,
    '#description' => t('Endpoint url.'),
  );

  $form['ilost_api']['size'] = array(
    '#type' => 'textfield',
    '#title' => t('Size'),
    '#default_value' => variable_get('ilost_api_size'),
    '#size' => 2,
    '#maxlength' => 2,
    '#description' => t('Affects the number of results returned and must be in the range 1-25 (inclusive). The size defaults to 10.'),
  );

  $form['ilost_custom'] = array(
    '#type' => 'fieldset',
    '#title' => t('Custom Settings'),
  );

  $form['ilost_custom']['note'] = array(
    '#title' => t('Note'),
    '#type' => 'textarea',
    '#description' => t('This text will be shown on the search page.'),
    '#default_value' => variable_get('ilost_note'),
  );

  $form['#submit'][] = 'ilost_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Form submission handler for search_admin_settings().
 */
function ilost_admin_settings_submit($form, &$form_state) {

  if ($form_state['values']['id']) {
    variable_set('ilost_organization_id', $form_state['values']['id']);
  }
  if ($form_state['values']['slug']) {
    variable_set('ilost_organization_slug', $form_state['values']['slug']);
  }
  if ($form_state['values']['url']) {
    variable_set('ilost_api_url', $form_state['values']['url']);
  }
  if ($form_state['values']['size']) {
    variable_set('ilost_api_size', $form_state['values']['size']);
  }
  if ($form_state['values']['note']) {
    variable_set('ilost_note', $form_state['values']['note']);
  }

}
